from django.contrib import admin
from .models import TodoList
from .models import TodoItem

# Register your models here.


# class TodoListAdmin(admin.ModelAdmin):
#     readonly_fields = ("id",)


class TodoListAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "created_on",
    )
    readonly_fields = ("id",)


class TodoItemAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "due_date",
        "is_completed",
        "task",
        "list",
    )
    readonly_fields = ("id",)


admin.site.register(TodoList, TodoListAdmin)
admin.site.register(TodoItem, TodoItemAdmin)
