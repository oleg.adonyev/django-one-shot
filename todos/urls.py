from django.urls import path
from todos import views


urlpatterns = [
    path(
        "",
        views.todo_list_list,
        name="todos.html",
    ),
    path("<int:id>/", views.show_todo, name="details.html"),
]
