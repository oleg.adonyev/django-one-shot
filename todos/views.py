from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList

# Create your views here.


def todo_list_list(request):
    todo = TodoList.objects.all()
    context = {"list_name": todo}
    return render(request, "todos.html", context)


def show_todo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {"todolists": todo}
    return render(request, "details.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoList(request.POST)
        if form.is_valid():
            create = form.save()
            return redirect("todo_list_detail", id=create.id)
    else:
        form = TodoList()

        context = {"form": form}
    return render(request, "todos.html", context)
